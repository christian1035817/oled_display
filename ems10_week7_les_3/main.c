#include <msp430.h>
#include <stdint.h>
#include <string.h>
#include "lib/gpio.h"
#include "lib/i2c.h"
#include "lib/oled.h"
#include "lib/fonts.h"
#include "lib/temperatuur.h"


// 1.6  SCL
// 1.7  SDA

void Rtag(int R);
void Gtag(int G);
void Btag(int B);
void tijd(int T);
void afstand(int S);


int T;


//#pragma vector = PORT2_VECTOR
//__interrupt void PORT2_isr(void)
//{
//    if ((P2IN & 1<<3))
//        {
//        tijd(T);
//        P2IFG = 0x00;
//        }
//  P2IFG = 0x00;
//}


void initDisplay();


void main(void) {
    WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer

    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation


    //R counter

    P2DIR &= ~1<<3;
    P2REN = 1<<3;
    P2OUT &= ~1<<3;

    P2IE |= 1<<3;
    P2IFG = 0x00;


    __enable_interrupt();

    /* Het display heeft na het aansluiten van de voedingsspanning even
     * de tijd nodig om op te starten voordat hij aangestuurd kan worden.
     * We wachten hier 20ms.
     */
    __delay_cycles(320000);

    initDisplay();


    while(1)
        {
            // Hoog fictieve temperatuur op met 0.1 graad
            // Geef dit weer op het display
            Rtag(0);
            Gtag(0);
            Btag(0);
            afstand(0);
            tijd(T++);
            __delay_cycles(10000000);
        }

        // Schrijf framebuffer naar het oleddisplay.

}
