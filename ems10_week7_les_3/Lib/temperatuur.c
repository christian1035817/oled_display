/*
 * temperatuur.c
 *
 *  Created on: 12 mrt. 2018
 *      Author: VersD
 */

#include <stdint.h>
#include <string.h>
#include "i2c.h"
#include "temperatuur.h"
#include "oled.h"
#include "fonts.h"

void initDisplay()
{
    oledInitialize();
    oledSetOrientation(FLIPPED);

    oledClearScreen();

    //Box buitenkant
            // Lijn in het midden
            oledFillBox(0, 0, 127, 0, 0x01);
            // Lijn onderaan
            oledFillBox(0, 7, 127, 7, 0x80);
            // Lijn rechts
            oledFillBox(127, 0, 127, 7, 0xFF);
            // Lijn links
            oledFillBox(0, 0, 0, 7, 0xFF);

    //Box van tags
            //verticale lijnen
            oledFillBox(77, 0, 77, 3, 0xFF);
            oledFillBox(93, 0, 93, 3, 0xFF);
            oledFillBox(111, 0, 111, 3, 0xFF);

            //horizontale lijnen boven
            oledFillBox(78, 2, 92, 2, 0x01);
            oledFillBox(94, 2, 110, 2, 0x01);
            oledFillBox(112, 2, 126, 2, 0x01);

            //horizontale lijnen beneden
            oledFillBox(78, 4, 92, 4, 0x01);
            oledFillBox(94, 4, 110, 4, 0x01);
            oledFillBox(112, 4, 126, 4, 0x01);

     //Tijd en afstand print
            oledPrint(2, 5, "Tijd:", small);
            oledPrint(2, 6, "Afstand:", small);
            oledPrint(83, 1, "R", small);
            oledPrint(100, 1, "G", small);
            oledPrint(117, 1, "B", small);
}

void Rtag(int R)
{
    // Twee char arrays voor de tekst
    char getal[4], decimalen[3];
    uint8_t x = 80, y = 3;


    //oude cijfers weghalen.
    oledClearBox(80, 3, 85, 3);

    //beperk mogelijkheden
    if (R > -100 && R < 1000)
    {


        // Tientallen omzetten naar string
        itoa(R / 1, getal);

        //eenheden omzetten naar string
        itoa(R % 1, decimalen);

        //print tientallen . eenheden
        oledPrint(x , y, getal, small);

    }
}

void Gtag(int G)
{
    // Twee char arrays voor de tekst
    char getal[4], decimalen[3];
    uint8_t x = 97, y = 3;


    //oude cijfers weghalen.
//    oledClearBox(11, 4, 59, 6);

    //beperk mogelijkheden
    if (G > -100 && G < 1000)
    {


        // Tientallen omzetten naar string
        itoa(G / 1, getal);

        //eenheden omzetten naar string
        itoa(G % 1, decimalen);

        //print tientallen . eenheden
        oledPrint(x , y, getal, small);

    }
}

void Btag(int B)
{
    // Twee char arrays voor de tekst
    char getal[4];
    uint8_t x = 115, y = 3;


    //oude cijfers weghalen.
//    oledClearBox(11, 4, 59, 6);

    //beperk mogelijkheden
    if (B > -100 && B < 1000)
    {


        // Tientallen omzetten naar string
        itoa(B / 1, getal);

        //print tientallen . eenheden
        oledPrint(x , y, getal, small);


    }
}

void tijd(int T)
{
    // Twee char arrays voor de tekst
       char getal[4];
       uint8_t x = 79, y = 5;
       uint8_t offset = 0;

       //oude cijfers weghalen.
       oledClearBox(25, 5, 120, 5);

       //beperk mogelijkheden
       if (T > -1 && T <  99999)
       {
           // Tientallen omzetten naar string
           itoa(T / 1, getal);



           //print tientallen . eenheden
           offset += oledPrint(x + offset, y, getal,     small);

           oledPrint(x + offset + 1, y, "sec",       small);
       }
}

void afstand(int S)
{
    // Twee char arrays voor de tekst
       char getal[4];
       uint8_t x = 79, y = 6;
       uint8_t offset = 0;

       //oude cijfers weghalen.
       oledClearBox(50, 6, 120, 6);

       //beperk mogelijkheden
       if (S > -1 && S <  99999)
       {
           // Tientallen omzetten naar string
           itoa(S / 1, getal);

           //print tientallen . eenheden
           offset += oledPrint(x + offset, y, getal,     small);
           oledPrint(x + offset + 1      , y, "m"  ,     small);
       }
}

void setTitle(char tekst[])
{
    // Print titel
    oledPrint(3, 0, tekst, big);
}

/* Aantal hulp functies voor getallen en tekst */
/* Draai array om */
static void reverse(char s[])
{
    int i, j ;
    char c ;

    // Tegelijk optellen en aftellen
    for (i = 0, j = strlen(s) - 1; i < j ; i++, j-- )
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/* itoa:  integer naar ascii omzetting */
void itoa(int n, char s[])
{
    int i, sign ;

    // als negatief
    if ((sign = n) < 0)
    {
        n = -n; //maak positief
    }

    i = 0;

    do
    {
        s[i++] = n % 10 + '0'; // Offset vanaf '0' character
    }
    while((n /= 10) > 0); // Delen door 10 en opnieuw

    // Als negatief
    if(sign < 0)
    {
        s[i++] = '-'; // Voeg - toe
    }

    s[i] = '\0'; // Eindigen string

    reverse(s); // Draai de boel om
}
