/*
 * temperatuur.h
 *
 *  Created on: 12 mrt. 2018
 *      Author: VersD
 */

#ifndef LIB_TEMPERATUUR_H_
#define LIB_TEMPERATUUR_H_
#include <stdint.h>

void initDisplay();

void R(int R);
void G(int G);
void B(int B);
void tijd(int T);
void afstand(int S);

void setTitle(char tekst[]);
static void reverse(char s[]);
void itoa(int n, char s[]);

#endif /* LIB_TEMPERATUUR_H_ */
